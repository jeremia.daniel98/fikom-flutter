import 'package:fikom/customColors.dart';
import 'package:fikom/advertisement/adv_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fikom/globals.dart' as global;
import 'dart:convert';

class AdvDetail extends StatelessWidget {
  final AdvCard data;

  const AdvDetail({this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: customGrey,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_outlined,
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: EdgeInsets.fromLTRB(24, 24, 16, 8),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      data.title,
                      style: TextStyle(
                          fontFamily: "CircularStd",
                          fontWeight: FontWeight.w500,
                          fontSize: 40),
                    ))),
            AspectRatio(
              child: Image.network(
                  "${global.http_url}/api/adv/img/${data.image}",
                  fit: BoxFit.contain),
              aspectRatio: 1.5 / 1,
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8, 24, 8, 8),
              child: RichText(
                text: TextSpan(
                    text: data.desc,
                    style: TextStyle(
                      color: Color(0xFF000000),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
