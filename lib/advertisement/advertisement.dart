import 'package:fikom/customColors.dart';
import 'package:fikom/advertisement/adv_card.dart';
import 'package:fikom/advertisement/adv_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fikom/globals.dart' as global;
import 'dart:convert';

class Adv extends StatefulWidget {
  final String title = "Advertisement";

  // about_connect(this.title);

  @override
  _AdvState createState() => _AdvState();
}

class _AdvState extends State<Adv> {
  List<AdvCard> lists = [
    // AdvCard(
    //     "Title",
    //     "Head of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect LabHead of Connect Lab",
    //     "Untitled.png"),
    // AdvCard("Title", "Head of Connect Lab", "Untitled.png"),
    // AdvCard("Title", "Head of Connect Lab", "Untitled.png"),
    // AdvCard("Title", "Head of Connect Lab", "Untitled.png"),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData().then((f) {
      setState(() {
        for (var item in f["res"]) {
          AdvCard card = AdvCard.fromJson(item);
          lists.add(card);
        }
        // _isLoading = false;
      });
    });
  }

  Future<Map<String, dynamic>> getData() async {
    try {
      var response = await http.get("${global.http_url}/api/adv");
      Map<String, dynamic> json = jsonDecode(response.body);
      return json;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: customGrey,
        body: Column(
          children: <Widget>[Expanded(child: AdvList(this.lists))],
        ));
  }
}

class AdvList extends StatefulWidget {
  final List<AdvCard> listItems;

  AdvList(this.listItems);

  @override
  _AdvListState createState() => _AdvListState();
}

class _AdvListState extends State<AdvList> {
  void bookmark(Function callback) {
    this.setState(() {
      callback();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.fromLTRB(24, 24, 16, 16),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Advertisements",
                  style: TextStyle(
                      fontFamily: "CircularStd",
                      fontWeight: FontWeight.w500,
                      fontSize: 25),
                ))),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: this.widget.listItems.length,
          itemBuilder: (context, index) {
            var card = this.widget.listItems[index];
            return Container(
              height: 250,
              child: Card(
                  margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        AspectRatio(
                          child: Image.network(
                              "${global.http_url}/api/adv/img/${card.image}",
                              fit: BoxFit.cover),
                          aspectRatio: 2 / 2.5,
                        ),
                        Expanded(
                            child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(8, 8, 0, 8),
                              child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    card.title,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: "CircularStd",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20),
                                  )),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(8, 8, 0, 8),
                              child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    card.desc,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 8,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: "CircularStd"),
                                  )),
                            ),
                            Spacer(),
                            Padding(
                                padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                        child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AdvDetail(data: card)),
                                        );
                                      },
                                      child: Text(
                                        "Continue reading >",
                                        style: TextStyle(
                                            color: customDarkBlue,
                                            fontFamily: "CircularStd"),
                                      ),
                                    )),
                                  ],
                                ))
                          ],
                        ))
                      ],
                    ),
                  )),
            );
          },
        )
      ],
    )));
  }
}
