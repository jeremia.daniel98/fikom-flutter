import 'package:fikom/globals.dart' as global;

class AdvCard {
  String title;
  String desc;
  String image;

  AdvCard(this.title, this.desc, this.image);
  factory AdvCard.fromJson(Map<String, dynamic> json) {
    return AdvCard(
      json["title"],
      json['desc'],
      json['image'],
    );
  }
}
