import 'package:fikom/customColors.dart';
import 'package:fikom/redaksiPeople.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fikom/globals.dart' as global;
import 'dart:convert';

class Redaksi extends StatefulWidget {
  final String title = "Redaksi";

  // about_connect(this.title);

  @override
  _RedaksiState createState() => _RedaksiState();
}

class _RedaksiState extends State<Redaksi> {
  List<RedaksiPeople> lists = [
    // RedaksiPeople("Mr Yenny", "Head of Connect Lab", "0873467234",
    //     "auwehf@gmail.com", "assets/cover14.png"),
    // RedaksiPeople("Mr Yenny", "Head of Connect Lab", "0873467234",
    //     "auwehf@gmail.com", "assets/cover14.png"),
    // RedaksiPeople("Mr Yenny", "Head of Connect Lab", "0873467234",
    //     "auwehf@gmail.com", "assets/cover14.png"),
    // RedaksiPeople("Mr Yenny", "Head of Connect Lab", "0873467234",
    //     "1234567890123456@peter.petra.ac.id", "assets/cover14.png"),
    // RedaksiPeople("Mr Yenny", "Head of Connect Lab", "0873467234",
    //     "auwehf@gmail.com", "assets/cover14.png"),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData().then((f) {
      setState(() {
        for (var item in f["res"]) {
          RedaksiPeople card = RedaksiPeople.fromJson(item);
          lists.add(card);
        }
        // _isLoading = false;
      });
    });
  }

  Future<Map<String, dynamic>> getData() async {
    try {
      var response = await http.get("${global.http_url}/api/redaksi");
      Map<String, dynamic> json = jsonDecode(response.body);
      return json;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: customGrey,
        body: Column(
          children: <Widget>[Expanded(child: RedaksiList(this.lists))],
        ));
  }
}

class RedaksiList extends StatefulWidget {
  final List<RedaksiPeople> listItems;

  RedaksiList(this.listItems);

  @override
  _RedaksiListState createState() => _RedaksiListState();
}

class _RedaksiListState extends State<RedaksiList> {
  void bookmark(Function callback) {
    this.setState(() {
      callback();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.fromLTRB(24, 24, 16, 16),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Connect's Team",
                  style: TextStyle(
                      fontFamily: "CircularStd",
                      fontWeight: FontWeight.w500,
                      fontSize: 25),
                ))),
        GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.listItems.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: MediaQuery.of(context).size.width /
                  (MediaQuery.of(context).size.height * 1),
            ),
            itemBuilder: (BuildContext context, int index) {
              var person = widget.listItems[index];
              return Padding(
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                          child: Text(
                            person.name,
                            style: TextStyle(fontFamily: "CircularStd"),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Divider(),
                        ),
                        AspectRatio(
                          child: Image.network(
                              "${widget.listItems[index].image}",
                              fit: BoxFit.cover),
                          aspectRatio: 2 / 2.5,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Divider(),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                          child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(person.jabatan)),
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
                                  child: Icon(Icons.email_outlined),
                                ),
                                Expanded(child: Text(person.email))
                              ],
                            )),
                      ],
                    ),
                  ));
            })
      ],
    )));
  }
}
