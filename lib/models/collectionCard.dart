import 'dart:io';
import 'package:fikom/customColors.dart';
import 'package:fikom/globals.dart' as global;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../detailPDF.dart';

class CollectionCard {
  int id;
  String title;
  String author;
  int edition;
  DateTime released;
  String image;
  String PDFlink;
  bool bookmarked = false;

  CollectionCard(this.id, this.title, this.author, this.edition, this.released,
      this.image, this.PDFlink, this.bookmarked);

  factory CollectionCard.fromJson(Map<String, dynamic> json) {
    return CollectionCard(
        json["id"],
        json['title'],
        json['author'],
        json['edition'],
        HttpDate.parse(json['released']),
        json['image'],
        "${global.http_url}/api/pdf/${json['PDFlink']}",
        false);
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "author": author,
        "edition": edition,
        "released": released,
        "image": image,
        "PDFlink": PDFlink,
        "bookmarked": bookmarked
      };

  void setBookmark() {
    this.bookmarked = !this.bookmarked;
  }

  bool getBookmark() {
    return this.bookmarked;
  }

  Widget getCard(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.network("${global.http_url}/api/img/$image",
                fit: BoxFit.contain),
            Expanded(
                child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.fromLTRB(4, 2, 8, 2),
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
                        decoration: BoxDecoration(
                          color: customDarkBlue,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(3),
                              child: Icon(
                                Icons.local_fire_department,
                                color: Colors.white,
                                size: 12,
                              ),
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child: Text(
                                  "NEW",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "CircularStd",
                                      fontSize: 12),
                                )),
                          ],
                        )),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "${global.months[released.month - 1]}, ${released.year}",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: "CircularStd"),
                            ))),
                    Container(
                      padding: EdgeInsets.fromLTRB(8, 2, 8, 2),
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Align(
                          alignment: Alignment.topRight,
                          child: Text(
                            "Edisi $edition",
                            style: TextStyle(
                                color: Colors.white, fontFamily: "CircularStd"),
                          )),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        title,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: "CircularStd",
                            fontWeight: FontWeight.w500,
                            fontSize: 20),
                      )),
                ),
                Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      author,
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: "CircularStd",
                          fontWeight: FontWeight.w500),
                    )),
                Spacer(),
                Padding(
                    padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => detailPDF(this)),
                            );
                          },
                          child: Text(
                            "Continue reading >",
                            style: TextStyle(
                                color: customDarkBlue,
                                fontFamily: "CircularStd"),
                          ),
                        )),
                        IconButton(
                            icon: bookmarked
                                ? Icon(Icons.bookmark)
                                : Icon(Icons.bookmark_border),
                            onPressed: () => setBookmark()),
                      ],
                    ))
              ],
            ))
          ],
        ),
      ),
    );
  }
}
