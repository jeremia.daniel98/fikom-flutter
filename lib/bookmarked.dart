import 'package:fikom/models/collectionCard.dart';
import 'package:fikom/customColors.dart';
import 'package:fikom/detailPDF.dart';
import 'package:fikom/Login/loginController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:fikom/globals.dart' as global;
import 'dart:convert';

import 'Login/login_screen.dart';

class Bookmarked extends StatefulWidget {
  final String title = "Bookmarked";

  // about_connect(this.title);

  @override
  _BookmarkedState createState() => _BookmarkedState();
}

class _BookmarkedState extends State<Bookmarked> {
  bool _isLoading = true;
  bool _isRefreshing = false;
  bool _isLatest = false;
  List<CollectionCard> lists = [];

  @override
  void initState() {
    _isLoading = true;
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var provider = Provider.of<LoginController>(context, listen: false);
      getLatestNews(provider).then((f) {
        setState(() {
          for (var item in f["res"]) {
            CollectionCard card = CollectionCard.fromJson(item);
            card.bookmarked = true;
            lists.add(card);
          }
          _isLoading = false;
        });
      });
    });
  }

  Future<Map<String, dynamic>> getLatestNews(provider) async {
    try {
      var response = await http.get("${global.http_url}/api/news/b",
          headers: {"Authorization": provider.session_id});
      Map<String, dynamic> json = jsonDecode(response.body);
      print(json);
      return json;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<LoginController>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: provider.isLoggedIn
          ? <Widget>[Expanded(child: CollectionList(this.lists))]
          : <Widget>[
              Center(
                child: Column(
                  children: [
                    Text("Please log in to access bookmark page"),
                    FlatButton(
                      child: Text("Login"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    LoginScreen()));
                      },
                    )
                  ],
                ),
              )
            ],
    );
  }
}

class CollectionList extends StatefulWidget {
  final List<CollectionCard> listItems;

  CollectionList(this.listItems);

  @override
  _CollectionListState createState() => _CollectionListState();
}

class _CollectionListState extends State<CollectionList> {
  final List<String> _months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  Future<bool> bookmarkToServer(provider, item) async {
    try {
      var res = http.post(
        "${global.http_url}/api/news/b/${item.id}",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{'session_id': provider.session_id}),
      );
      return true;
    } catch (e) {
      return false;
    }
  }

  void bookmark(Function callback) {
    this.setState(() {
      callback();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: widget.listItems.length != 0
            ? SingleChildScrollView(
                child: Column(
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.fromLTRB(24, 24, 16, 0),
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Bookmarked",
                            style: TextStyle(
                                fontFamily: "CircularStd",
                                fontWeight: FontWeight.w500,
                                fontSize: 25),
                          ))),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: this.widget.listItems.length,
                    itemBuilder: (context, index) {
                      var card = this.widget.listItems[index];
                      return Container(
                        height: 250,
                        child: Card(
                            margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Image.network(
                                      "${global.http_url}/api/img/${card.image}",
                                      fit: BoxFit.contain),
                                  Expanded(
                                      child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                              child: Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "${_months[card.released.month - 1]}, ${card.released.year}",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontFamily:
                                                            "CircularStd"),
                                                  ))),
                                          Container(
                                            padding:
                                                EdgeInsets.fromLTRB(8, 2, 8, 2),
                                            decoration: BoxDecoration(
                                              color: Colors.black,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            child: Align(
                                                alignment: Alignment.topRight,
                                                child: Text(
                                                  "Edisi ${card.edition}",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontFamily:
                                                          "CircularStd"),
                                                )),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 8, 0, 8),
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              card.title,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontFamily: "CircularStd",
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 20),
                                            )),
                                      ),
                                      Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Text(
                                            card.author,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "CircularStd",
                                                fontWeight: FontWeight.w500),
                                          )),
                                      Spacer(),
                                      Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(8, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  child: GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            detailPDF(card)),
                                                  );
                                                },
                                                child: Text(
                                                  "Continue reading >",
                                                  style: TextStyle(
                                                      color: customDarkBlue,
                                                      fontFamily:
                                                          "CircularStd"),
                                                ),
                                              )),
                                              IconButton(
                                                  icon: card.bookmarked
                                                      ? Icon(Icons.bookmark)
                                                      : Icon(Icons
                                                          .bookmark_border),
                                                  onPressed: () {
                                                    var provider = Provider.of<
                                                            LoginController>(
                                                        context,
                                                        listen: false);
                                                    if (provider.isLoggedIn) {
                                                      this
                                                          .bookmarkToServer(
                                                              provider, card)
                                                          .then((value) {
                                                        if (value) {
                                                          this.bookmark(
                                                              card.setBookmark);
                                                        }
                                                      });
                                                    }
                                                  }),
                                            ],
                                          ))
                                    ],
                                  ))
                                ],
                              ),
                            )),
                      );
                    },
                  )
                ],
              ))
            : Center(child: Text("No Bookmarked Newsletter Yet!")));
  }
}
