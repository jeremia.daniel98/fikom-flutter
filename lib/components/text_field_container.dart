import 'package:flutter/material.dart';
import 'package:fikom/constants.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  final Color lightcolor;
  const TextFieldContainer({
    Key key,
    this.child,
    this.lightcolor = kPrimaryLightColor
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: lightcolor,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}
