import 'package:flutter/material.dart';
import 'package:fikom/components/text_field_container.dart';
import 'package:fikom/constants.dart';
import 'package:flutter/gestures.dart';

class RoundedCPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final Color color;
  const RoundedCPasswordField({
    Key key,
    this.onChanged,
    this.color = kPrimaryLightColor
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          hintText: "Confirm Password",
          icon: Icon(
            Icons.lock,
            color: kPrimaryColor,
          ),
          border: InputBorder.none,
        ),
      ),
      lightcolor: color,
    );
  }
}
