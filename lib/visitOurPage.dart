import 'package:fikom/constants.dart';
import 'package:fikom/models/collectionCard.dart';
import 'package:fikom/customColors.dart';
import 'package:fikom/detailPDF.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class VOP extends StatefulWidget {
  final String title = "Visit Our Page";

  // about_connect(this.title);

  @override
  _VOPState createState() => _VOPState();
}

class _VOPState extends State<VOP> {

  List<String> url = [
    "https://www.instagram.com/labmedia.ikom/", //instagram
    "https://www.youtube.com/channel/UCIZfgYVkLBd_X40ONcBmUIw", //youtube
  ];

  void _launchURL(url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: customGrey,
        body: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(24, 24, 16, 0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Check Our Amazing Pages!",
                      style: TextStyle(
                          fontFamily: "CircularStd",
                          fontWeight: FontWeight.w500,
                          fontSize: 25),
                    ))),
            GridView.count(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              crossAxisCount: 2,
              children: <Widget>[
                new GestureDetector(
                  onTap: (){
                    _launchURL(url[0]);
                  },
                  child:
                  Container(
                    margin: EdgeInsets.all(20),
                    padding: EdgeInsets.all(50),
                    child: Center(
                      child: Image.asset(
                          "assets/icons/instagram.png"
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: kPrimaryLightColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(2),
                          topRight: Radius.circular(2),
                          bottomLeft: Radius.circular(2),
                          bottomRight: Radius.circular(2)
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 2,
                          blurRadius: 5,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  ),
                ),
                new GestureDetector(
                  onTap: (){
                    _launchURL(url[1]);
                  },
                  child:
                  Container(
                    margin: EdgeInsets.all(20),
                    padding: EdgeInsets.all(50),
                    child: Center(
                      child: Image.asset(
                          "assets/icons/youtube.png"
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: kPrimaryLightColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(2),
                          topRight: Radius.circular(2),
                          bottomLeft: Radius.circular(2),
                          bottomRight: Radius.circular(2)
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 2,
                          blurRadius: 5,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),

          ],
        ));
  }
}
