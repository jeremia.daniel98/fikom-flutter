import 'package:fikom/customColors.dart';
import 'package:fikom/redaksi.dart';
import 'package:fikom/visitOurPage.dart';
import 'package:fikom/advertisement/advertisement.dart';
import 'package:fikom/newspaper/news.dart';
import 'package:fikom/Login/login_screen.dart';
import 'package:fikom/Login/loginController.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'about_connect.dart';
import 'bookmarked.dart';
import 'collection.dart';
import 'myDrawer.dart';

void main() => runApp(MyApp());

class CustomScaffold extends StatefulWidget {
  Widget appBar;
  Widget body;
  Widget drawer;

  CustomScaffold({Widget appBar, Widget body, Widget drawer}) {
    this.appBar = appBar;
    this.body = body;
    this.drawer = drawer;
  }

  @override
  _CustomScaffoldState createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var provider = Provider.of<LoginController>(context, listen: false);
      provider.loadFromStorage();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: widget.appBar, body: widget.body, drawer: widget.drawer);
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int index = 0;
  List<Widget> list = [
    Collection(), //0
    Bookmarked(), //1
    AboutConnect(), //2
    Redaksi(), //3
    VOP(), //4
    Adv(), //5
    News() //6
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => LoginController(),
      child: Builder(
        builder: (context) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              home: Scaffold(
                  backgroundColor: customGrey,
                  resizeToAvoidBottomPadding: false,
                  body: Consumer<LoginController>(
                    builder: (context, provider, child) {
                      return CustomScaffold(
                        appBar: AppBar(
                          backgroundColor: customGrey,
                          centerTitle: true,
                          title: Text(
                            "Connect",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w900),
                          ),
                          iconTheme: IconThemeData(color: Colors.black),
                        ),
                        body: list[index],
                        drawer: MyDrawer(
                          onTap: (ctx, i) {
                            setState(() {
                              index = i;
                              Navigator.pop(ctx);
                            });
                          },
                        ),
                      );
                    },
                  )));
        },
      ),
    );
  }
}
