import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF035178);
const kPrimaryLightColor = Color(0xFFd4f1ff);
const kErrorColor = Color(0xFFffcdd2);