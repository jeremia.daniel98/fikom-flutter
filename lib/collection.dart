import 'package:fikom/Login/loginController.dart';
import 'package:fikom/models/collectionCard.dart';
import 'package:fikom/customColors.dart';
import 'package:fikom/detailPDF.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fikom/globals.dart' as global;
import 'dart:convert';

import 'package:provider/provider.dart';

class Collection extends StatefulWidget {
  final String title = "Collection";

  // about_connect(this.title);

  @override
  _CollectionState createState() => _CollectionState();
}

class _CollectionState extends State<Collection> {
  bool _isLoading = true;
  bool _isRefreshing = false;
  bool _isLatest = false;
  List<CollectionCard> lists = [];

  @override
  void initState() {
    _isLoading = true;
    super.initState();

    var temp_user = null;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var provider = Provider.of<LoginController>(context, listen: false);
      if (provider.isLoggedIn) {
        temp_user = provider.userData;
      }
    });

    getLatestNews().then((f) {
      setState(() {
        print("Collection Init");
        print(temp_user);
        for (var item in f["res"]) {
          CollectionCard card = CollectionCard.fromJson(item);
          if (temp_user != null) {
            if (temp_user["bookmarked_newsletters"].contains(card.id)) {
              card.bookmarked = true;
            }
          }
          lists.add(card);
        }
        _isLoading = false;
      });
    });
  }

  @override
  void onResumed() {
    print("AAAAAAAAAAAAAAA");
    var temp_user = null;
    lists = [];
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var provider = Provider.of<LoginController>(context, listen: false);
      if (provider.isLoggedIn) {
        temp_user = provider.userData;
      }
    });

    getLatestNews().then((f) {
      setState(() {
        for (var item in f["res"]) {
          print("sesuati");
          CollectionCard card = CollectionCard.fromJson(item);
          if (temp_user != null) {
            if (temp_user["bookmarked_newsletters"].contains(card.id)) {
              card.bookmarked = true;
            }
          }
          lists.add(card);
        }
        _isLoading = false;
      });
    });
  }

  void refreshPage() {
    _isLoading = true;
    var temp_user = null;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var provider = Provider.of<LoginController>(context, listen: false);
      if (provider.isLoggedIn) {
        temp_user = provider.userData;
      }
    });

    getLatestNews().then((f) {
      setState(() {
        print(f);
        for (var item in f["res"]) {
          CollectionCard card = CollectionCard.fromJson(item);
          if (temp_user != null) {
            if (temp_user["bookmarked_newsletters"].contains(card.id)) {
              card.bookmarked = true;
            }
          }
          lists.add(card);
        }
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<LoginController>(context);
    return Scaffold(
        backgroundColor: customGrey,
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Column(
                children: <Widget>[
                  Expanded(
                      child: NotificationListener<ScrollNotification>(
                    // ignore: missing_return
                    onNotification: (ScrollNotification scrollInfo) {
                      if (!_isLoading &&
                          !_isRefreshing &&
                          !_isLatest &&
                          scrollInfo.metrics.pixels ==
                              scrollInfo.metrics.maxScrollExtent) {
                        setState(() {
                          _isRefreshing = true;
                        });
                        this.getNewsUpdate().then((f) {
                          setState(() {
                            for (var item in f["res"]) {
                              lists.add(CollectionCard.fromJson(item));
                            }
                            _isRefreshing = false;
                            if (f["res"].length == 0) _isLatest = true;
                          });
                        });
                      }
                    },
                    child: this.lists.isEmpty
                        ? Text("No news available")
                        : CollectionList(this.lists, _isRefreshing, _isLatest),
                  )),
                ],
              ));
  }

  Future<Map<String, dynamic>> getLatestNews() async {
    try {
      var response = await http.get("${global.http_url}/api/news/l/");
      Map<String, dynamic> json = jsonDecode(response.body);
      return json;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<Map<String, dynamic>> getNewsUpdate() async {
    try {
      var response =
          await http.get("${global.http_url}/api/news/l/${lists.last.edition}");
      Map<String, dynamic> json = jsonDecode(response.body);
      return json;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

class CollectionList extends StatefulWidget {
  final List<CollectionCard> listItems;
  final bool isRefreshing;
  final bool isLatest;

  CollectionList(this.listItems, this.isRefreshing, this.isLatest);

  @override
  _CollectionListState createState() => _CollectionListState();
}

class _CollectionListState extends State<CollectionList> {
  final List<String> _months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  void bookmark(Function callback) {
    this.setState(() {
      callback();
    });
  }

  Future<bool> bookmarkToServer(provider, item) async {
    try {
      var res = http.post(
        "${global.http_url}/api/news/b/${item.id}",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{'session_id': provider.session_id}),
      );
      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.fromLTRB(24, 24, 16, 0),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "New Release",
                  style: TextStyle(
                      fontFamily: "CircularStd",
                      fontWeight: FontWeight.w500,
                      fontSize: 25),
                ))),
        Container(
          height: 250,
          child: Card(
            margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.network(
                      "${global.http_url}/api/img/${widget.listItems.first.image}",
                      fit: BoxFit.contain),
                  Expanded(
                      child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.fromLTRB(4, 2, 8, 2),
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
                              decoration: BoxDecoration(
                                color: customDarkBlue,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(3),
                                    child: Icon(
                                      Icons.local_fire_department,
                                      color: Colors.white,
                                      size: 12,
                                    ),
                                  ),
                                  Align(
                                      alignment: Alignment.topRight,
                                      child: Text(
                                        "NEW",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "CircularStd",
                                            fontSize: 12),
                                      )),
                                ],
                              )),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "${_months[widget.listItems.first.released.month - 1]}, ${widget.listItems.first.released.year}",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: "CircularStd"),
                                  ))),
                          Container(
                            padding: EdgeInsets.fromLTRB(8, 2, 8, 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Align(
                                alignment: Alignment.topRight,
                                child: Text(
                                  "Edisi ${widget.listItems.first.edition}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "CircularStd"),
                                )),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              widget.listItems.first.title,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: "CircularStd",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20),
                            )),
                      ),
                      Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            widget.listItems.first.author,
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: "CircularStd",
                                fontWeight: FontWeight.w500),
                          )),
                      Spacer(),
                      Padding(
                          padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            detailPDF(widget.listItems.first)),
                                  );
                                },
                                child: Text(
                                  "Continue reading >",
                                  style: TextStyle(
                                      color: customDarkBlue,
                                      fontFamily: "CircularStd"),
                                ),
                              )),
                              IconButton(
                                  icon: widget.listItems.first.bookmarked
                                      ? Icon(Icons.bookmark)
                                      : Icon(Icons.bookmark_border),
                                  onPressed: () {
                                    var provider = Provider.of<LoginController>(
                                        context,
                                        listen: false);
                                    if (provider.isLoggedIn) {
                                      this
                                          .bookmarkToServer(
                                              provider, widget.listItems.first)
                                          .then((value) {
                                        if (value) {
                                          this.bookmark(widget
                                              .listItems.first.setBookmark);
                                        }
                                      });
                                    }
                                  }),
                            ],
                          ))
                    ],
                  ))
                ],
              ),
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(24, 24, 16, 0),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Collection",
                  style: TextStyle(
                      fontFamily: "CircularStd",
                      fontWeight: FontWeight.w500,
                      fontSize: 25),
                ))),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: this.widget.listItems.length,
          itemBuilder: (context, index) {
            if (index == 0) return SizedBox(height: 2);
            var card = this.widget.listItems[index];
            return Container(
              height: 250,
              child: Card(
                  margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Image.network(
                            "${global.http_url}/api/img/${card.image}",
                            fit: BoxFit.contain),
                        Expanded(
                            child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "${_months[card.released.month - 1]}, ${card.released.year}",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "CircularStd"),
                                        ))),
                                Container(
                                  padding: EdgeInsets.fromLTRB(8, 2, 8, 2),
                                  decoration: BoxDecoration(
                                    color: Colors.black,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Align(
                                      alignment: Alignment.topRight,
                                      child: Text(
                                        "Edisi ${card.edition}",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "CircularStd"),
                                      )),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                              child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    card.title,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: "CircularStd",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20),
                                  )),
                            ),
                            Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  card.author,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: "CircularStd",
                                      fontWeight: FontWeight.w500),
                                )),
                            Spacer(),
                            Padding(
                                padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                        child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  detailPDF(card)),
                                        );
                                      },
                                      child: Text(
                                        "Continue reading >",
                                        style: TextStyle(
                                            color: customDarkBlue,
                                            fontFamily: "CircularStd"),
                                      ),
                                    )),
                                    IconButton(
                                        icon: card.bookmarked
                                            ? Icon(Icons.bookmark)
                                            : Icon(Icons.bookmark_border),
                                        onPressed: () {
                                          var provider =
                                              Provider.of<LoginController>(
                                                  context,
                                                  listen: false);
                                          if (provider.isLoggedIn) {
                                            this
                                                .bookmarkToServer(
                                                    provider, card)
                                                .then((value) {
                                              if (value) {
                                                this.bookmark(card.setBookmark);
                                              }
                                            });
                                          }
                                        }),
                                  ],
                                ))
                          ],
                        ))
                      ],
                    ),
                  )),
            );
          },
        ),
        Container(
          height: widget.isRefreshing ? 50.0 : 0,
          color: Colors.white70,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
        Container(
          height: widget.isLatest ? 50.0 : 0,
          color: Colors.white70,
          child: Center(
            child: new Text("No more newsletter available!"),
          ),
        )
      ],
    )));
  }
}
