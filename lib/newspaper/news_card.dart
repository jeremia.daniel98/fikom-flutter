import 'package:fikom/globals.dart' as global;

class NewsCard {
  String title;
  String desc;
  String image;

  NewsCard(this.title, this.desc, this.image);
  factory NewsCard.fromJson(Map<String, dynamic> json) {
    return NewsCard(
      json["title"],
      json['desc'],
      json['image'],
    );
  }
}
