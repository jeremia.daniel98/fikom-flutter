import 'package:flutter/material.dart';

import 'customColors.dart';

class AboutConnect extends StatelessWidget {
  final String title = "About Connect";

  // about_connect(this.title);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: customGrey,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(24, 24, 16, 0),
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "About Connect",
                        style: TextStyle(
                            fontFamily: "CircularStd",
                            fontWeight: FontWeight.w500,
                            fontSize: 25),
                      ))),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: RichText(
                  text: TextSpan(
                      text: '''Salam''',
                      style: TextStyle(
                        color: Color(0xFF000000),
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: ''' Fikomers!
                    ''',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text: '''
\t\t\t\t\t\t\t\tUntuk dapat terus menghubungkan antara alumni dan program studi, Laboratorium Media Fikom UK Petra berinovasi dengan mengembangkan aplikasi Connect, agar memudahkan''',
                          style: TextStyle(
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text: ''' Fikomers ''',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text:
                              '''mendapatkan informasi up-to-date berkaitan dengan Ilmu Komunikasi.''',
                          style: TextStyle(
                            color: Color(0xFF000000),
                          ),
                        ),
                      ]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Image(
                    image: AssetImage("assets/images/pic1.jpg"), width: size.width),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: RichText(
                  text: TextSpan(
                      text:
                          '''\t\t\t\t\t\t\t\tTerbit perdana April 2018, Connect punya beragam rubrik yang ringkas, padat, dan kaya akan informasi. ''',
                      style: TextStyle(
                        color: Color(0xFF000000),
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: '''Fikomers ''',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text:
                              '''bisa mendapatkan beragam cerita mengenai profil alumnus, beragam bidang kerja ilmu komunikasi, hingga kisah menarik yang ditulis oleh alumnus, di setiap edisinya.
                      ''',
                          style: TextStyle(
                            color: Color(0xFF000000),
                          ),
                        ),
                      ]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Image(
                    image: AssetImage("assets/images/pic2.JPG"), width: size.width),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: RichText(
                  text: TextSpan(
                    text:
                        '''\t\t\t\t\t\t\t\tConnect juga menjadi sarana bagi mahasiswa untuk berkarya dan belajar.  Dengan demikian, ketika mengikuti program magang ataupun memasuki dunia kerja, mereka dapat memiliki portofolio yang lengkap serta beragam, khususnya dalam bidang penulisan.  ''',
                    style: TextStyle(
                      color: Color(0xFF000000),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Image(
                    image: AssetImage("assets/images/pic3.JPG"), width: size.width),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: RichText(
                  text: TextSpan(
                      text:
                          '''\t\t\t\t\t\t\t\tSelain Connect, Lab. Media juga memiliki program lain seperti konten Instagram, YouTube, hingga ''',
                      style: TextStyle(
                        color: Color(0xFF000000),
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: '''workshop ''',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text:
                              '''menarik yang diselenggarakan secara berkala. Kunjungi ''',
                          style: TextStyle(
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text: '''platform ''',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text: '''Laboratorium Media lainnya di menu ''',
                          style: TextStyle(
                            color: Color(0xFF000000),
                          ),
                        ),
                        TextSpan(
                          text: '''Visit Our Page ''',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Color(0xFF000000),
                          ),
                        ),
                      ]),
                ),
              )
            ],
          ),
        ));
  }
}
