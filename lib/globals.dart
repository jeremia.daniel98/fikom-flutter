// const String http_url = "http://10.0.2.2";
const String http_url = "http://connect.petra.ac.id";

final List<String> months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
