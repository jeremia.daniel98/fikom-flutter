import 'package:fikom/globals.dart' as global;

class RedaksiPeople {
  String name;
  String jabatan;
  String email;
  String image;

  RedaksiPeople(this.name, this.jabatan, this.email, this.image);
  factory RedaksiPeople.fromJson(Map<String, dynamic> json) {
    return RedaksiPeople(
      json["name"],
      json['jabatan'],
      json['email'],
      "${global.http_url}/api/r/img/${json['image']}",
    );
  }
}
