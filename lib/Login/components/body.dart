import 'package:fikom/Login/loginController.dart';
import 'package:fikom/SignUp/signup_screen.dart';
import 'package:fikom/components/text_field_container.dart';
import 'package:fikom/constants.dart';
import 'package:fikom/customColors.dart';
import 'package:flutter/material.dart';
import 'package:fikom/Login/components/background.dart';

// import 'package:fikom/Screens/Signup/signup_screen.dart';
import 'package:fikom/components/already_have_an_account_acheck.dart';
import 'package:fikom/components/rounded_button.dart';
import 'package:fikom/components/rounded_input_field.dart';
import 'package:fikom/components/rounded_password_field.dart';
import 'package:provider/provider.dart';
import 'package:flutter/gestures.dart';

class Body extends StatefulWidget {
  Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String username;
  String password;
  Color color = kPrimaryLightColor;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var provider = Provider.of<LoginController>(context);
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.03),
            Image(
                image: AssetImage("assets/icons/logo_black.png"),
                width: size.width * 0.75),
            SizedBox(height: size.height * 0.1),
            RoundedInputField(
              hintText: "Username",
              onChanged: (value) {
                setState(() {
                  username = value;
                });
              },
              color: color,
            ),
            RoundedPasswordField(
              onChanged: (value) {
                setState(() {
                  password = value;
                });
              },
              color: color,
            ),
            RoundedButton(
              text: "LOGIN",
              press: () {
                provider.login(username, password).then((value) {
                  if (value) {
                    Navigator.pop(context);
                  }
                  else {
                    setState(() {
                      color = kErrorColor;
                    });
                    final snackBar = SnackBar(
                      content: Text('Could not login, please try again!'),
                    );
                    Scaffold.of(context).showSnackBar(snackBar);
                  }
                });
              },
            ),
            RichText(
                text: TextSpan(
                    style: TextStyle(
                        fontFamily: "CircularStd",
                        fontSize: 15,
                        color: Colors.black),
                    children: <TextSpan>[
                  new TextSpan(text: "Don't have an account? "),
                  new TextSpan(
                    text: "Sign up",
                    style: new TextStyle(color: customDarkBlue),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () => {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        SignUpScreen()))
                          },
                  ),
                ])),
            SizedBox(height: size.height * 0.03),
            // AlreadyHaveAnAccountCheck(
            //   press: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) {
            //           return SignUpScreen();
            //         },
            //       ),
            //     );
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
