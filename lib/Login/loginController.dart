import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fikom/globals.dart' as global;

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginController with ChangeNotifier {
  Map<String, dynamic> userData;
  bool isLoggedIn = false;
  String session_id;

  final storage = new FlutterSecureStorage();

  Future<bool> login(String username, String password) async {
    try {
      print(username);
      print(password);
      var response = await http.post(
        "${global.http_url}/api/login",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
            <String, String>{'username': username, 'password': password}),
      );
      Map<String, dynamic> json = jsonDecode(response.body);
      print(json);
      if (json.isNotEmpty) {
        if (json["err_code"] == 0) {
          isLoggedIn = true;
          session_id = json["session_id"];
          userData = json["res"];
          print(userData);
          await storage.write(key: "session_id", value: session_id);
          await storage.write(key: "userData", value: jsonEncode(userData));
          await storage.write(key: "isLoggedIn", value: isLoggedIn ? "1" : "0");
          notifyListeners();
          return true;
        } else
          return false;
      } else {
        return false;
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<bool> loadFromStorage() async {
    var loggedIn = await storage.read(key: "isLoggedIn");
    if (loggedIn.isNotEmpty) {
      isLoggedIn = true;
      session_id = await storage.read(key: "session_id");
      userData = jsonDecode(await storage.read(key: "userData"));
      return true;
    }
    return false;
  }

  void logout() async {
    try {
      var response = await http.post("${global.http_url}/api/logout",
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{'session_id': session_id}));
      isLoggedIn = false;
      session_id = null;
      userData = null;

      notifyListeners();
    } catch (e) {
      throw Exception(e.toString());
    }
    await storage.delete(key: "session_id");
    await storage.delete(key: "userData");
    await storage.delete(key: "isLoggedIn");
  }

  Map<String, dynamic> getUserData(String session_id) {}

  void testLogin() {
    isLoggedIn = !isLoggedIn;
    notifyListeners();
  }

  Future<Map<String, dynamic>> signup(String username, String password) async {
    try {
      var response = await http.post(
        "${global.http_url}/api/user",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'username': username,
          'name': username,
          'password': password
        }),
      );
      Map<String, dynamic> json = jsonDecode(response.body);
      return json;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}
