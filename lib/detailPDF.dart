import 'dart:io';
import 'dart:async';
import 'dart:typed_data';

import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:fikom/models/collectionCard.dart';
import 'package:fikom/customColors.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';

class detailPDF extends StatefulWidget {
  final CollectionCard data;

  detailPDF(this.data);

  @override
  _detailPDFState createState() => _detailPDFState();
}

class _detailPDFState extends State<detailPDF> {
  String assetPDFpath = "";
  String urlPDFpath = "";
  // PDFDocument document;
  bool _isLoading = true;
  File pdf_file = null;

  final Completer<PDFViewController> _controller =
      Completer<PDFViewController>();
  int pages = 0;
  int currentPage = 0;
  bool isReady = false;
  String errorMessage = "";

  @override
  void initState() {
    super.initState();
    // loadDocument();
    // test();
    createFileOfPdfUrl().then((f) {
      setState(() {
        urlPDFpath = f.path;
        pdf_file = f;
        _isLoading = false;
      });
    });
    // setState(() {
    //   urlPDFpath = "/data/user/0/com.example.fikom/app_flutter/pedoman2020_v1.pdf";
    // });
  }

  // loadDocument() async {
  //   document = await PDFDocument.fromURL(
  //       "https://ukpinfor.petra.ac.id/pf/pedoman2020_v1.pdf");
  //
  //     setState(() => _isLoading = false);
  // }

  // void test() async {
  //   var dir = await getApplicationDocumentsDirectory();
  //   File f = File("${dir.path}/test.txt");
  //   await f.writeAsString("Testtesttest");
  //
  //   File r = File("${dir.path}/test.txt");
  //   var content = await r.readAsString();
  //   print(content);
  // }

  Future<File> createFileOfPdfUrl() async {
    Completer<File> completer = Completer();

    print("Start download file from internet!");
    try {
      var url = widget.data.PDFlink;
      var filename = url.substring(url.lastIndexOf("/") + 1);

      var response = await http.get(url);
      var dir = await getApplicationDocumentsDirectory();
      print("Download files");
      print("${dir.path}/$filename");
      File file = File("${dir.path}/$filename");

      // Check if file exists
      var exist = await file.exists();
      if (exist) {
        completer.complete(file);
        return completer.future;
      }

      await file.writeAsBytes(response.bodyBytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  void writeFile() async {
    // storage permission ask
    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    }
    // the downloads folder path
    Directory tempDir = await DownloadsPathProvider.downloadsDirectory;
    String tempPath = tempDir.path;
    var filename = pdf_file.path.substring(pdf_file.path.lastIndexOf("/") + 1);
    var filePath = tempPath + '/$filename';
    var bytes = await pdf_file.readAsBytes();
    // save the data in the path
    File(filePath).writeAsBytes(bytes, flush: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: customGrey,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_outlined,
            color: Colors.black,
          ),
        ),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: Builder(
                builder: (context) => GestureDetector(
                  onTap: () {
                    if (pdf_file != null) {
                      print("downloading pdf to phone");
                      final snackBar = SnackBar(
                        content: Text(
                            "PDF Downloading in background. File will be saved in Downloads folder"),
                        duration: const Duration(seconds: 10),
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                      writeFile();
                    }
                  },
                  child: Icon(
                    Icons.download_rounded,
                    color: Colors.black,
                  ),
                ),
              )),
        ],
      ),
      body: Stack(
        children: <Widget>[
          _isLoading
              ? Center(child: CircularProgressIndicator())
              : PDFView(
                  filePath: urlPDFpath,
                  enableSwipe: true,
                  swipeHorizontal: false,
                  autoSpacing: false,
                  pageFling: true,
                  pageSnap: true,
                  defaultPage: currentPage,
                  fitPolicy: FitPolicy.BOTH,
                  preventLinkNavigation:
                      false, // if set to true the link is handled in flutter
                  onRender: (_pages) {
                    setState(() {
                      pages = _pages;
                      isReady = true;
                    });
                  },
                  onError: (error) {
                    setState(() {
                      errorMessage = error.toString();
                    });
                    print(error.toString());
                  },
                  onPageError: (page, error) {
                    setState(() {
                      errorMessage = '$page: ${error.toString()}';
                    });
                    print('$page: ${error.toString()}');
                  },
                  onViewCreated: (PDFViewController pdfViewController) {
                    _controller.complete(pdfViewController);
                  },
                  onLinkHandler: (String uri) {
                    print('goto uri: $uri');
                  },
                  onPageChanged: (int page, int total) {
                    print('page change: $page/$total');
                    setState(() {
                      currentPage = page;
                    });
                  },
                ),
          errorMessage.isEmpty
              ? !isReady
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : Container()
              : Center(
                  child: Text(errorMessage),
                )
        ],
      ),
    );
  }
}
