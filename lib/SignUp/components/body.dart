import 'package:fikom/Login/loginController.dart';
import 'package:fikom/Login/login_screen.dart';
import 'package:fikom/components/rounded_cpassword_field.dart';
import 'package:fikom/constants.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fikom/Login/components/background.dart';
// import 'package:fikom/Screens/Signup/signup_screen.dart';
import 'package:fikom/components/already_have_an_account_acheck.dart';
import 'package:fikom/components/rounded_button.dart';
import 'package:fikom/components/rounded_input_field.dart';
import 'package:fikom/components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../customColors.dart';

class Body extends StatefulWidget {
  Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String username;
  String password;
  String c_password;
  Color _color = kPrimaryLightColor;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var provider = Provider.of<LoginController>(context);
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.03),
            Image(
                image: AssetImage("assets/icons/logo_black.png"),
                width: size.width * 0.75),
            SizedBox(height: size.height * 0.1),
            RoundedInputField(
              hintText: "Username",
              onChanged: (value) {
                setState(() {
                  username = value;
                });
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                setState(() {
                  password = value;
                  if (c_password != password) {
                    _color = kErrorColor;
                  } else {
                    _color = kPrimaryLightColor;
                  }
                });
              },
              color: _color,
            ),
            RoundedCPasswordField(
              onChanged: (value) {
                setState(() {
                  c_password = value;
                  if (c_password != password) {
                    _color = kErrorColor;
                  } else {
                    _color = kPrimaryLightColor;
                  }
                });
              },
              color: _color,
            ),
            RoundedButton(
              text: "SIGN UP",
              press: () {
                if (c_password == password) {
                  provider.signup(username, password).then((value) {
                    print(value);
                    if (value['err_code'] == 0)
                      Navigator.pop(context);
                    else {
                      final snackBar = SnackBar(
                        content: Text(value['msg']),
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                    }
                  });
                } else {
                  setState(() {
                    _color = kErrorColor;
                  });
                }
              },
            ),
            RichText(
                text: TextSpan(
                    style: TextStyle(
                        fontFamily: "CircularStd",
                        fontSize: 15,
                        color: Colors.black),
                    children: <TextSpan>[
                  new TextSpan(text: "Already have an account? "),
                  new TextSpan(
                    text: "Log in",
                    style: new TextStyle(color: customDarkBlue),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () => {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        LoginScreen()))
                          },
                  ),
                ])),
            SizedBox(height: size.height * 0.03),
            // AlreadyHaveAnAccountCheck(
            //   press: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) {
            //           return SignUpScreen();
            //         },
            //       ),
            //     );
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
