import 'package:fikom/Login/loginController.dart';
import 'package:fikom/Login/login_screen.dart';
import 'package:fikom/SignUp/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'customColors.dart';
import 'package:fikom/components/rounded_button.dart';

class MyDrawer extends StatefulWidget {
  String name = "Anonymous";
  String email = "Anonymous";

  int _currentSelected = -1;
  final Function onTap;

  MyDrawer({this.onTap});

  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  void movePages(int selected) {
    this.setState(() {
      widget._currentSelected = selected;
      widget.onTap(context, selected);
    });
  }

  List<Widget> getAnon(BuildContext ctx) {
    return <Widget>[
      Container(
        // width: 60,
        height: 60,
        child: Row(
          children: [
            CircleAvatar(
              backgroundColor: Colors.blueGrey,
            ),
            SizedBox(
              width: 10,
            ),
            FlatButton(
              child: Text('Login'),
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(
                    ctx,
                    MaterialPageRoute(
                        builder: (BuildContext context) => LoginScreen()));
              },
            ),
            FlatButton(
              child: Text('Sign Up'),
              onPressed: () {
                Navigator.push(
                    ctx,
                    MaterialPageRoute(
                        builder: (BuildContext context) => SignUpScreen()));
              },
            )
          ],
        ),
      ),
    ];
  }

  List<Widget> getLoggedIn(BuildContext ctx, provider) {
    return <Widget>[
      Container(
        // width: 60,
        height: 60,
        child: Row(
          children: [
            CircleAvatar(
              backgroundColor: Colors.blueGrey,
            ),
            FlatButton(
              child: Text("Logout"),
              onPressed: () {
                provider.logout();
                Navigator.pop(ctx);
              },
            )
          ],
        ),
      ),
      SizedBox(
        height: 15,
      ),
      Text(
        provider.userData["name"],
        // "Name",
        style: TextStyle(
            fontFamily: "CircularStd",
            fontWeight: FontWeight.w700,
            fontSize: 30),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<LoginController>(context);
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.8,
      child: Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(color: Colors.white),
                child: Padding(
                  padding: EdgeInsets.all(6),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: provider.isLoggedIn
                          ? getLoggedIn(context, provider)
                          : getAnon(context)),
                )),
            ListTile(
              leading: Icon(Icons.menu_book_sharp,
                  color: widget._currentSelected == 0
                      ? customDarkBlue
                      : Colors.black),
              title: Text(
                "Collection",
                style: TextStyle(
                    fontFamily: "CircularStd",
                    fontWeight: FontWeight.w700,
                    color: widget._currentSelected == 0
                        ? customDarkBlue
                        : Colors.black),
              ),
              onTap: () {
                movePages(0);
              },
            ),
            ListTile(
              leading: Icon(Icons.bookmark,
                  color: widget._currentSelected == 1
                      ? customDarkBlue
                      : Colors.black),
              title: Text(
                "Bookmarked",
                style: TextStyle(
                    fontFamily: "CircularStd",
                    fontWeight: FontWeight.w700,
                    color: widget._currentSelected == 1
                        ? customDarkBlue
                        : Colors.black),
              ),
              onTap: () {
                movePages(1);
              },
            ),
            Divider(
              height: 1,
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
              child: Text("Our Other Content",
                  style: TextStyle(
                      fontFamily: "CircularStd",
                      fontWeight: FontWeight.w700,
                      color: Colors.grey)),
            ),
            ListTile(
              leading: Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: Image.asset(
                  "assets/Ads.png",
                ),
              ),
              title: Text(
                "Advertising",
                style: TextStyle(
                    fontFamily: "CircularStd",
                    fontWeight: FontWeight.w700,
                    color: widget._currentSelected == 5
                        ? customDarkBlue
                        : Colors.black),
              ),
              onTap: () {
                this.movePages(5);
              },
            ),
            ListTile(
              leading: Icon(Icons.web_outlined,
                  color: widget._currentSelected == 6
                      ? customDarkBlue
                      : Colors.black),
              title: Text(
                "News",
                style: TextStyle(
                    fontFamily: "CircularStd",
                    fontWeight: FontWeight.w700,
                    color: widget._currentSelected == 6
                        ? customDarkBlue
                        : Colors.black),
              ),
              onTap: () {
                this.movePages(6);
              },
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
              child: Text("Connect",
                  style: TextStyle(
                      fontFamily: "CircularStd",
                      fontWeight: FontWeight.w700,
                      color: Colors.grey)),
            ),
            ListTile(
              leading: Icon(Icons.people,
                  color: widget._currentSelected == 2
                      ? customDarkBlue
                      : Colors.black),
              title: Text(
                "About Connect",
                style: TextStyle(
                    fontFamily: "CircularStd",
                    fontWeight: FontWeight.w700,
                    color: widget._currentSelected == 2
                        ? customDarkBlue
                        : Colors.black),
              ),
              onTap: () {
                this.movePages(2);
              },
            ),
            ListTile(
              leading: Icon(Icons.star,
                  color: widget._currentSelected == 3
                      ? customDarkBlue
                      : Colors.black),
              title: Text(
                "Redaksi",
                style: TextStyle(
                    fontFamily: "CircularStd",
                    fontWeight: FontWeight.w700,
                    color: widget._currentSelected == 3
                        ? customDarkBlue
                        : Colors.black),
              ),
              onTap: () {
                this.movePages(3);
              },
            ),
            ListTile(
              leading: Padding(
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                child: Image.asset(
                  "assets/visitus.png",
                  color: Colors.black,
                ),
              ),
              title: Text(
                "Visit Out Page",
                style: TextStyle(
                    fontFamily: "CircularStd",
                    fontWeight: FontWeight.w700,
                    color: widget._currentSelected == 4
                        ? customDarkBlue
                        : Colors.black),
              ),
              onTap: () {
                this.movePages(4);
              },
            ),
          ],
        ),
      ),
    );
  }
}
